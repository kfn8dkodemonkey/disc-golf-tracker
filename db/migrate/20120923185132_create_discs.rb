class CreateDiscs < ActiveRecord::Migration
  def change
    create_table :discs do |t|
      t.integer(:disc_id)
      t.integer(:user_id, :null => false)
      t.integer(:name, :null => false)
      t.integer(:style, :null => false)
      t.integer(:brand, :null => false)
      t.integer(:plastic, :null => false)
      t.integer(:color, :null => false)
      t.integer(:stability, :null => true)
      t.integer(:weight, :null => true)
      t.integer(:level, :null => true)
      t.timestamps
    end

    add_index(:discs, :user_id)

  end
end