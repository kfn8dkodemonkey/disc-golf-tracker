# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

DiscColor.delete_all
DiscColor.create([
  { :color => "Clear" },
  { :color => "White" },
  { :color => "Beige" },
  { :color => "Yellow" },
  { :color => "Bright Yellow" }
], :without_protection => true )
