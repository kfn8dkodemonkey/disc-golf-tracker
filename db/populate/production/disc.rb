Disc.delete_all
Disc.create([
  {:disc_id => nil},
  {:user_id => nil},
  {:name => nil},
  {:disc_style => nil},
  {:brand => nil},
  {:plastic => nil},
  {:color => nil},
  {:stability => nil},
  {:weight => nil},
  {:level => nil},
  {:created_at => nil},
  {:updated_at => nil }
], :without_protection => true )
