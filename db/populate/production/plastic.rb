DiscPlastic.delete_all
DiscPlastic.create([
  { :plastic => "Star Line" },
  { :plastic => "Echo Star Line" },
  { :plastic => "Champion" },
  { :plastic => "Blizzard Champion" },
  { :plastic => "Pro Line" }
], :without_protection => true )
