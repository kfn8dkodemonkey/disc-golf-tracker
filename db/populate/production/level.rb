DiscLevel.delete_all
DiscLevel.create([
  { :level => "Entry" },
  { :level => "Professional" },
  { :level => "Premium" },
  { :level => "Super-Premium" }
], :without_protection => true )
