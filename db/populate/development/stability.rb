DiscStability.delete_all
DiscStability.create([
  { :stability => "Overstable" },
  { :stability => "Massively Overstable" },
  { :stability => "Stable" },
  { :stability => "Understable" },
  { :stability => "Veru Understable" }
], :without_protection => true )
