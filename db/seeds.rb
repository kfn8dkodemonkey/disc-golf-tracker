# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Disc.create([
  { :disc_id => nil, :userid => nil, :name => nil, :disc_style => nil, :brand => nil, :plastic => nil, :color => nil, :stability => nil, :weight => nil, :level => nil, :created_at => nil, :updated_at => nil }
], :without_protection => true )


DiscBrand.create([
  { :brand => "Aerobie"},
  { :brand => "Ching"},
  { :brand => "DGA" },
  { :brand => "Disc Mania" },
  { :brand => "Discraft" }
], :without_protection => true )



DiscColor.create([
  { :color => "Clear" },
  { :color => "White" },
  { :color => "Beige" },
  { :color => "Yellow" },
  { :color => "Bright Yellow" }
], :without_protection => true )



DiscLevel.create([
  { :level => "Entry" },
  { :level => "Professional" },
  { :level => "Premium" },
  { :level => "Super-Premium" }
], :without_protection => true )



DiscName.create([
  { :name => "Boss" },
  { :name => "LED Flight" },
  { :name => "Katana" },
  { :name => "Buzz" },
  { :name => "Wraith" }
], :without_protection => true )


DiscPlastic.create([
  { :plastic => "Star Line" },
  { :plastic => "Echo Star Line" },
  { :plastic => "Champion" },
  { :plastic => "Blizzard Champion" },
  { :plastic => "Pro Line" }
], :without_protection => true )



DiscStability.create([
  { :stability => "Overstable" },
  { :stability => "Massively Overstable" },
  { :stability => "Stable" },
  { :stability => "Understable" },
  { :stability => "Veru Understable" }
], :without_protection => true )



DiscStyle.create([
  { :style => "Driver" },
  { :style => "Mid Rang" },
  { :style => "Putter" }
], :without_protection => true )



DiscWeight.create([
  { :weight => 139 },
  { :weight => 140 },
  { :weight => 150 },
  { :weight => 160 },
  { :weight => 165 }
], :without_protection => true )




