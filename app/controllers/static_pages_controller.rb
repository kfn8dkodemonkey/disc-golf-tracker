class StaticPagesController < ApplicationController
  def courses
  end

  def rules
  end

  def discstyle
  end

  def throwingstyle
  end

  def coursecomponents
  end

  def scoring
  end

  def credits
  end
end
