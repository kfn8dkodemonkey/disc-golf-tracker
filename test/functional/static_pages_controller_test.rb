require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get courses" do
    get :courses
    assert_response :success
  end

  test "should get rules" do
    get :rules
    assert_response :success
  end

  test "should get discstyle" do
    get :discstyle
    assert_response :success
  end

  test "should get throwingstyle" do
    get :throwingstyle
    assert_response :success
  end

  test "should get coursecomponents" do
    get :coursecomponents
    assert_response :success
  end

  test "should get scoring" do
    get :scoring
    assert_response :success
  end

  test "should get credits" do
    get :credits
    assert_response :success
  end

end
